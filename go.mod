module gitlab.com/easy_bakery/platter

go 1.14

require (
	github.com/hashicorp/consul/api v1.7.0
	github.com/sirupsen/logrus v1.7.0
)
