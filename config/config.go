package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/easy_bakery/platter/logger"

	consulApi "github.com/hashicorp/consul/api"
)

type Settings struct {
	ConsulURL   string
	Prefix      string
	AppName     string
	AutoRefresh bool
	PollingTime float32
}

type Config struct {
	Keys map[string]string
}

func BuildFromJSON(settings Settings) (c *Config, log *logger.Log) {
	log = logger.NewLogger(settings.AppName)
	c = &Config{}
	jsonFile, err := ioutil.ReadFile("env.json")
	err = json.Unmarshal(jsonFile, &c.Keys)

	if err != nil {
		log.Error(fmt.Sprintf("Could not load json config file, check your 'env.json' file"), err)
	}

	return c, log
}

func Build(settings Settings, keys []string) (c *Config, log *logger.Log) {
	var consulConfig consulApi.Config
	c = &Config{}
	log = logger.NewLogger(settings.AppName)
	c.Keys = make(map[string]string)

	consulConfig = consulApi.Config{
		Address: settings.ConsulURL,
	}

	client, err := consulApi.NewClient(&consulConfig)

	if err != nil {
		log.Error("[ERROR] An error has happened on call Consul", err)
	}

	kv := client.KV()

	for _, key := range keys {
		keyWith := fmt.Sprintf("%s/%s/%s", settings.Prefix, settings.AppName, key)
		pair, _, err := kv.Get(keyWith, nil)
		if err != nil {
			log.Error("[CONFIG] Impossible to get key value in consul", err)
			c.Keys[key] = os.Getenv(key)
			log.Info(fmt.Sprintf("[CONFIG] Getting key value %s from os env", os.Getenv(key)))
		} else if pair == nil {
			log.Info(fmt.Sprintf("[CONFIG] There is no such key named '%s' in consul", key))
		} else {
			result := c.byteToString(pair.Value)
			c.Keys[key] = result
		}
	}

	c.Dumps(settings.AppName)

	if settings.AutoRefresh {
		minutes := time.Duration(settings.PollingTime*60) * time.Minute
		if minutes > 0 {
			time.AfterFunc(minutes, func() {
				go Build(settings, keys)
			})
		} else {
			time.AfterFunc(5*time.Minute, func() {
				go Build(settings, keys)
			})
		}

	}

	return c, log
}

func (c Config) byteToString(bs []byte) string {
	ba := make([]byte, 0, len(bs))
	for _, b := range bs {
		ba = append(ba, byte(b))
	}
	return string(ba)
}

// Getting from: https://stackoverflow.com/a/48150584
func (c Config) createKeyValuePairs(m map[string]string) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		_, _ = fmt.Fprintf(b, "%s=\"%s\"\n", key, value)
	}
	return b.String()
}

func (c Config) Dumps(appName string) {
	data, err := json.Marshal(c.Keys)
	log := logger.NewLogger(appName)

	if err != nil {
		panic(err)
	}

	log.Info(fmt.Sprintf("[CONFIG] %s", string(data)))
}
