EASY_BAKERY_DOCKER_IMAGE = "easy-bakery-bff"
EASY_BAKERY_CONTAINER_NAME = $(EASY_BAKERY_DOCKER_IMAGE)_instance
CONSUL_DOCKER_IMAGE = "consul"
CONSUL_CONTAINER_NAME = $(CONSUL_DOCKER_IMAGE)_instance
NETWORK_NAME = "network_test"

create_docker_network:
	docker network create $(NETWORK_NAME)

clean:
	docker container rm $$(docker ps -aq) -f

build:
	docker build -t $(EASY_BAKERY_DOCKER_IMAGE) .

run: build
	docker run -d --publish 8080:8000 --name $(EASY_BAKERY_CONTAINER_NAME) --network $(NETWORK_NAME) --rm $(EASY_BAKERY_DOCKER_IMAGE)

consul_dev:
	docker run -d -p 8500:8500 -p 8600:8600/udp --network $(NETWORK_NAME) --name=$(CONSUL_CONTAINER_NAME) $(CONSUL_DOCKER_IMAGE):v0.6.4 agent -server -bootstrap -ui -client=0.0.0.0
