FROM golang:1.14

#Example: Getting environment variables into application context.
ENV foo my_foo_value
ENV bar my_bar_value
ENV CONSUL_URL consul_instance:8500

RUN go get -v github.com/sirupsen/logrus
RUN go get -v github.com/google/uuid
RUN go get -v github.com/hashicorp/consul/api

ADD . /go/src/platter
RUN go install platter
ENTRYPOINT /go/bin/platter

EXPOSE 8000